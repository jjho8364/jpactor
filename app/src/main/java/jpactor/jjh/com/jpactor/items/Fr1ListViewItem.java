package jpactor.jjh.com.jpactor.items;

/**
 * Created by Administrator on 2016-06-18.
 */
public class Fr1ListViewItem {
    String imgUrl;
    private String text1;
    private String text2;

    public Fr1ListViewItem(String imgUrl, String text1, String text2) {
        this.imgUrl = imgUrl;
        this.text1 = text1;
        this.text2 = text2;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }
}
