package jpactor.jjh.com.jpactor.fragment;

import android.app.ProgressDialog;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import jpactor.jjh.com.jpactor.R;
import jpactor.jjh.com.jpactor.adapter.Fr1ListViewAdapter;
import jpactor.jjh.com.jpactor.items.Fr1ListViewItem;
import jpactor.jjh.com.jpactor.utils.NetworkUtil;

/**
 * Created by Administrator on 2016-06-11.
 */
public class TabFragment1 extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private View view = null;
    private ProgressDialog mProgressDialog;
    private PopupWindow mPopupWindow;
    private Button btn1;
    private Button btn2;
    private String currentUrl = "http://graviainterview.tistory.com/category/2016%EB%85%84%20%EC%B6%9C%EC%8B%9C%EC%9E%91";
    private String btn1Url = "";
    private String btn2Url = "";
    private ArrayList<String> categoryArr;
    private ArrayList<String> urlArr;
    private int selectedBtn = 1;
    private ArrayList<Fr1ListViewItem> listArray;
    private ListView listView;
    //private Fr1ListViewAdapter adapter;
    private boolean utorrent = false;
    private boolean torrentSearcher = false;
    AdView mAdViewTop;
    AdRequest adRequest1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        String model = Build.MODEL.toLowerCase();
        if(model.equals("sph-d720") || model.contains("nexus") /*|| model.contains("lg")*/){
            view = inflater.inflate(R.layout.activity_main2, container, false);
        } else {
            view = inflater.inflate(R.layout.tab_fragment_1, container, false);
            super.onCreate(savedInstanceState);

            mAdViewTop = (AdView)view.findViewById(R.id.adViewTop);
            adRequest1 = new AdRequest.Builder().build();
            mAdViewTop.loadAd(adRequest1);

            // 초기화
            selectedBtn = 1;
            btn1 = (Button)view.findViewById(R.id.frag1_btn1);
            btn2 = (Button)view.findViewById(R.id.frag1_btn2);
            btn1.setOnClickListener(this);
            btn2.setOnClickListener(this);
            btn1.setBackgroundResource(R.drawable.btn_press);

            listArray = new ArrayList<Fr1ListViewItem>();

            listView = (ListView)view.findViewById(R.id.fr1_listview);
            listView.setAdapter(new Fr1ListViewAdapter(listArray, getActivity().getLayoutInflater()));   // context, item(layout), item(data)
            listView.setOnItemClickListener(this);

            if(NetworkUtil.getConnectivity(getContext())){
                new initBtnCategory().execute();
            } else {
                Toast.makeText(getActivity(), "네트워크 연결 상태를 확인 하세요.", Toast.LENGTH_SHORT).show();
            }

        }


        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        setUserVisibleHint(true);
    }

    // 클릭 리스너
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frag1_btn1:
                //Log.d("clicked ", " btn1");
                if(selectedBtn != 1){
                    selectedBtn = 1;
                    //new getGridView().execute();
                    btn1.setBackgroundResource(R.drawable.btn_press);
                    btn2.setBackgroundResource(R.drawable.btn_normal);

                    if(NetworkUtil.getConnectivity(getContext())){
                        new getListView().execute();
                    } else {
                        Toast.makeText(getActivity(), "네트워크 연결 상태를 확인 하세요.", Toast.LENGTH_SHORT).show();
                    }

                }
                break;
            case R.id.frag1_btn2:
                //Log.d("clicked ", " btn2");
                if(selectedBtn != 2){
                    selectedBtn = 2;
                    //new getGridView().execute();
                    btn2.setBackgroundResource(R.drawable.btn_press);
                    btn1.setBackgroundResource(R.drawable.btn_normal);

                    if(NetworkUtil.getConnectivity(getContext())){
                        new getListView().execute();
                    } else {
                        Toast.makeText(getActivity(), "네트워크 연결 상태를 확인 하세요.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.gridview_item, null);
        mPopupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        ImageView imageView = (ImageView)popupView.findViewById(R.id.gridViewItemImage);
        TextView textViewSeq = (TextView)popupView.findViewById(R.id.gridViewItemSeq);
        TextView textViewDetail = (TextView)popupView.findViewById(R.id.gridViewItemDetail);
        ImageView download = (ImageView)popupView.findViewById(R.id.fr1_grid_down);

        Glide.with(getActivity()).load(listArray.get(position).getImgUrl()).into(imageView);
        textViewSeq.setText(listArray.get(position).getText1());
        textViewDetail.setText(listArray.get(position).getText2());

        mPopupWindow.setBackgroundDrawable(new BitmapDrawable()) ;   // <== 팝업외 영역 터치시 반응하기 위해서 선언
        mPopupWindow.setOutsideTouchable(true);  //<== Hard 키에 반응하기 위해서 선언
        mPopupWindow.setFocusable(true);
        mPopupWindow.setAnimationStyle(-1); // 애니메이션 설정(-1:설정, 0:설정안함)
        mPopupWindow.showAtLocation(popupView, Gravity.CENTER, 0, -100);

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "곧 업데이트", Toast.LENGTH_SHORT).show();

                mPopupWindow.dismiss();

                //com.utorrent.client
                PackageManager pm = getContext().getPackageManager();
                List<ApplicationInfo> list = pm.getInstalledApplications(0);
                for (ApplicationInfo applicationInfo : list) {
                    String pName = applicationInfo.packageName;   // 앱 패키지
                    if(pName.equals("com.utorrent.client")){
                        utorrent = true;
                        break;
                    }
                }
                for (ApplicationInfo applicationInfo : list) {
                    String pName = applicationInfo.packageName;   // 앱 패키지
                    if(pName.equals("you.jjh.com.torrentsearcher")){
                        torrentSearcher = true;
                        break;
                    }
                }

                Log.d("installed utorrent - ", utorrent + "");
                Log.d("installed searcher - ", torrentSearcher + "");


            }
        });


    }

    public class initBtnCategory extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            categoryArr = new ArrayList<String>();
            urlArr = new ArrayList<String>();

            Document doc = null;

            try{
                doc = Jsoup.connect(currentUrl).get();
                Elements table = doc.select("#searchList ol");
                Elements rows = table.select("li a");

                for (Element row : rows) {
                    categoryArr.add(row.text());
                    urlArr.add(row.attr("href"));
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            btn1.setText(categoryArr.get(0));       // 버튼에 가져온 텍스트 추가
            if(categoryArr.size() != 1){
                btn2.setText(categoryArr.get(1));   // 버튼에 가져온 텍스트 추가
            }

            btn1Url = getString(R.string.base_url) + urlArr.get(0);
            if(urlArr.size() != 1){
                btn2Url = getString(R.string.base_url) + urlArr.get(1);
            }
            Log.d("btn1Url : ", btn1Url);
            Log.d("btn2Url : ", btn2Url);

            new getListView().execute();
        }
    }
    //////////////////////////////////////////////////////////
    public class getListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            listArray = null;
            listArray = new ArrayList<Fr1ListViewItem>();
            String url = "";
            if(selectedBtn == 1){
                url = btn1Url;
            } else {
                url = btn2Url;
            }

            Document doc = null;

            try {
                doc = Jsoup.connect(url).get();

                Elements table = doc.select("table.txc-table tbody");
                Elements rows = table.select("tr");

                int z=0;
                for (Element row : rows) {
                    if (z == 0) {
                        z++;
                        continue;
                    }

                    Elements tds = row.getElementsByTag("TD");

                    for (Element td : tds) {
                        Elements urlAddr = td.select("img[src]");
                        Elements p2 = td.select("p:nth-child(2)");
                        Elements p3 = td.select("p:nth-child(3)");
                        listArray.add(new Fr1ListViewItem(urlAddr.attr("src"), p2.text(), p3.text()));
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            listView.setAdapter(new Fr1ListViewAdapter(listArray, getActivity().getLayoutInflater()));

            mProgressDialog.dismiss();
        }
    }
}
