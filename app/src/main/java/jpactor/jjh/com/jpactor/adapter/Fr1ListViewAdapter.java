package jpactor.jjh.com.jpactor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jpactor.jjh.com.jpactor.R;
import jpactor.jjh.com.jpactor.items.Fr1ListViewItem;

/**
 * Created by Administrator on 2016-06-18.
 */

public class Fr1ListViewAdapter extends BaseAdapter {
    //Context context;
    //int layout;
    private LayoutInflater inflater;
    private ArrayList<Fr1ListViewItem> listViewItem;

    public Fr1ListViewAdapter(ArrayList<Fr1ListViewItem> listViewItem, LayoutInflater inflater) {
        this.inflater = inflater;
        this.listViewItem = listViewItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.fr1_listview_item, null);
        if(convertView != null){
            TextView seq = (TextView) convertView.findViewById(R.id.fr1_item_seq);
            TextView detail = (TextView) convertView.findViewById(R.id.fr1_item_detail);

            Fr1ListViewItem data = listViewItem.get(position);

            seq.setText(data.getText1());
            detail.setText(data.getText2());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return listViewItem.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
