package jpactor.jjh.com.jpactor.items;

/**
 * Created by Administrator on 2016-06-18.
 */
public class Fr2ListViewItem {
    String imaUrl;
    String krName;
    String jpName;
    String enName;
    String birth;
    String body;
    String cupAndDebut;

    public Fr2ListViewItem(String imaUrl, String krName, String jpName, String enName, String birth, String body, String cupAndDebut) {
        this.imaUrl = imaUrl;
        this.krName = krName;
        this.jpName = jpName;
        this.enName = enName;
        this.birth = birth;
        this.body = body;
        this.cupAndDebut = cupAndDebut;
    }
    public Fr2ListViewItem(String krName, String jpName, String enName, String birth, String body, String cupAndDebut) {
        this.krName = krName;
        this.jpName = jpName;
        this.enName = enName;
        this.birth = birth;
        this.body = body;
        this.cupAndDebut = cupAndDebut;
    }

    public String getImaUrl() {
        return imaUrl;
    }

    public void setImaUrl(String imaUrl) {
        this.imaUrl = imaUrl;
    }

    public String getKrName() {
        return krName;
    }

    public void setKrName(String krName) {
        this.krName = krName;
    }

    public String getJpName() {
        return jpName;
    }

    public void setJpName(String jpName) {
        this.jpName = jpName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCupAndDebut() {
        return cupAndDebut;
    }

    public void setCupAndDebut(String cupAndDebut) {
        this.cupAndDebut = cupAndDebut;
    }
}
