package jpactor.jjh.com.jpactor.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.ads.*;

import jpactor.jjh.com.jpactor.R;
import jpactor.jjh.com.jpactor.adapter.PagerAdapter;
import jpactor.jjh.com.jpactor.utils.NetworkUtil;

public class MainActivity extends AppCompatActivity {

    private String TAG = "MainActivity - ";
    private InterstitialAd interstitialAd;
    private boolean networkState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        //AdView mAdViewTop = (AdView) findViewById(R.id.adViewTop);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        //mAdViewTop.loadAd(adRequest);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/1008704852");
        interstitialAd.loadAd(adRequest);

        networkState = NetworkUtil.getConnectivity(this);
        Log.d(TAG, "NetworkUtil : " + networkState);

        tabInit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(interstitialAd.isLoaded()){
            interstitialAd.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void tabInit(){
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("신작"));
        tabLayout.addTab(tabLayout.newTab().setText("순위"));
        //tabLayout.addTab(tabLayout.newTab().setText("배우"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager)findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
        });
    }


}
