package jpactor.jjh.com.jpactor.adapter;

/**
 * Created by Administrator on 2016-06-19.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jpactor.jjh.com.jpactor.R;
import jpactor.jjh.com.jpactor.items.Fr2ListViewItem;

/**
 * Created by Administrator on 2016-06-18.
 */
public class Fr2ListViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Fr2ListViewItem> listArr;
    private LayoutInflater inflater;

    public Fr2ListViewAdapter(ArrayList<Fr2ListViewItem> listArr, LayoutInflater inflater, Context context) {
        this.listArr = listArr;
        this.inflater = inflater;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.fr2_listview_item, null);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.fr2_listview_image);
        TextView krName = (TextView) convertView.findViewById(R.id.fr2_listview_krname);
        TextView jpName = (TextView) convertView.findViewById(R.id.fr2_listview_jpname);
        TextView enName = (TextView) convertView.findViewById(R.id.fr2_listview_enname);
        TextView birth = (TextView) convertView.findViewById(R.id.fr2_listview_birth);
        TextView body = (TextView) convertView.findViewById(R.id.fr2_listview_body);
        TextView cupAndDebut = (TextView) convertView.findViewById(R.id.fr2_listview_cupanddebut);
        TextView rank = (TextView) convertView.findViewById(R.id.fr2_listview_rank);

        Fr2ListViewItem data = listArr.get(position);
        //////////////////////////////////////////////////////////////////
        if(listArr.get(position).getImaUrl()==null || !listArr.get(position).getImaUrl().equals("")){
            Glide.with(context).load(listArr.get(position).getImaUrl()).into(imageView);
        }
        krName.setText(data.getKrName());
        jpName.setText(data.getJpName());
        enName.setText(data.getEnName());
        birth.setText(data.getBirth());
        body.setText(data.getBody());
        cupAndDebut.setText(data.getCupAndDebut());
        rank.setText((position+1)+"");

        return convertView;
    }

    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
