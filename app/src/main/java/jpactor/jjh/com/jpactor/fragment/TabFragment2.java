package jpactor.jjh.com.jpactor.fragment;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import java.util.ArrayList;

import jpactor.jjh.com.jpactor.R;
import jpactor.jjh.com.jpactor.adapter.Fr2ListViewAdapter;
import jpactor.jjh.com.jpactor.items.Fr2ListViewItem;
import jpactor.jjh.com.jpactor.utils.NetworkUtil;

/**
 * Created by Administrator on 2016-06-11.
 */
public class TabFragment2 extends Fragment {
    private View view = null;
    private TextView tv_title;  // 타이틀 바
    private String title;

    private ArrayList<Fr2ListViewItem> listArray;
    private ListView listView;
    private Fr2ListViewAdapter adapter;

    ArrayList<String> list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab_fragment_2, container, false);
        super.onCreate(savedInstanceState);

        // 초기화
        tv_title = (TextView)view.findViewById(R.id.frag2_title);
        listArray = new ArrayList<Fr2ListViewItem>();
        listView = (ListView)view.findViewById(R.id.fr2_listview);
        listView.setAdapter(new Fr2ListViewAdapter(listArray, getActivity().getLayoutInflater(),getActivity()));


        if(NetworkUtil.getConnectivity(getContext())){
            new getRankTitle().execute();   // 랭크 타이틀 가져오기
        } else {
            Toast.makeText(getActivity(), "네트워크 연결 상태를 확인 하세요.", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    public class GetRankList extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            listArray = null;
            listArray = new ArrayList<Fr2ListViewItem>();
            list = new ArrayList<String>();
            Document doc = null;

            try {

                doc= Jsoup.connect("http://hentaku.net/rank/").userAgent("Chrome").get();
                Elements table = doc.select("#main .post");

                for(int i=1 ; i<101 ; i++){
                    Elements divs = table.select("div:nth-child(" + (i + 2) + ") .avstar_info_b");
                    list.add(divs.html() + "");
                }
                // info 저장
                for(int j=0 ; j<list.size() ; j++){
                    String all = list.get(j).replace("\n", "");
                    String temp[] = all.split("<br>");
                    temp[0] = temp[0].replace("<b>", "");
                    temp[0] = temp[0].replace("</b>", "");
                    temp[5] = temp[5].split("<a")[0];
                    listArray.add(new Fr2ListViewItem(temp[0],temp[1],temp[2],temp[3],temp[4],temp[5]));
                }
                // image url 저장
                for(int i=1 ; i<101 ; i++){
                    Elements div = table.select("div:nth-child(" + (i + 2) + ") .imgshow a img");
                    listArray.get(i-1).setImaUrl(div.attr("src"));
                }

            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            listView.setAdapter(new Fr2ListViewAdapter(listArray, getActivity().getLayoutInflater(), getActivity()));
        }
    }

    public class getRankTitle extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try{
                Document doc= Jsoup.connect("http://hentaku.net/rank/").userAgent("Chrome").get();
                Elements table = doc.select("#main .post");
                Elements divs = table.select("div:nth-child(1) h2");
                title = divs.text().split("AV신작")[0];
            }catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("123123", "" + title);
            if(title == null || title.equals("")){
                tv_title.setText("잠시 후 다시 이용해 주세요.");
            } else {
                tv_title.setText(title + "배우랭킹 TOP 100");
                new GetRankList().execute();
            }

        }
    }

}